import axios from "axios";
import {Reservation} from "./entities";

export const ReservationService = {

    async fetchOneReservation(id:number|string) {
        const response = await axios.get<Reservation>('api/reservation/'+id);
        return response.data;
    },
    
    async fetchAllReservation() {
        const response = await axios.get<Reservation[]>('/api/reservation');
        return response.data;
    },

    async fetchReservationsByUser (id:number|string){
        const response = await axios.get<Reservation[]>('api/reservation/user/'+id)
        return response.data;
    },

    async postReservation(reservation: any) {
        const response = await axios.post<any>("/api/reservation", reservation);
        return response.data;
      }
}

