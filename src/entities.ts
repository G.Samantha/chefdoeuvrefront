export interface Animal{
    id?:number;
    name:string;
    img:string;
    description:string;
}
export interface Circuit{
    id?:number;
    name:string;
    img:string;
    description:string;
}

export interface User {
    id?: number;
    name?: string;
    lastname?: string;
    email: string;
    password: string;
    phone?: string;
    reservation: Reservation[];
    role?: string
}

export interface Reservation {
    id?:number,
    date?:string;
    animal?:Animal;
    circuit?:Circuit;
    user?:User;
}