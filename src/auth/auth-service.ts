import {  User } from "@/entities";
import axios from "axios";


export async function login(email: string, password: string) {
    const response = await axios.post<{ token: string }>('/api/login', { email, password });
    return response.data.token;
}


export async function fetchUser() {
    const response = await axios.get<User>('/api/account');
    return response.data;
}


export async function fetchAdmin() {
    const response = await axios.get<User>('/api/user/promote');
    return response.data;
}

export async function updateUser(User:User) {
    const response = await axios.patch<User>('/api/account/'+User.id, User);
    return response.data;
}
