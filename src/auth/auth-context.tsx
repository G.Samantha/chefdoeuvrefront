import { setCookie, destroyCookie, parseCookies } from "nookies";
import { createContext, useEffect, useState } from "react";

// un "contrat" passer avec la classe définissant les propritétés et méthodes minimum requise pour qu'elle soit valide
interface AuthState {
  token?: string | null;
  setToken: (value: string | null) => void;
}

export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
  const [token, setToken] = useState<string | null>("");

  function handleSet(value: string | null) {
    if (value) {
      setCookie(null, "token", value);
    } else {
      destroyCookie(null, "token");
    }
    setToken(value);
  }

  // Quand l'user arrive sur le site, le useEffect vérifie si il y a un cookie valide ou non
  useEffect(() => {
    setToken(parseCookies().token);
  }, []);

  return (
    <AuthContext.Provider value={{ token, setToken: handleSet }}>
      {children}
    </AuthContext.Provider>
  );
};
