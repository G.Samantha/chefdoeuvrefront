import axios from "axios";
import { parseCookies } from "nookies";

axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;

axios.interceptors.request.use((config) => {
    const { token } = parseCookies();
    if (token) {
        config.headers.setAuthorization('Bearer ' + token);
    }
    return config;
})
