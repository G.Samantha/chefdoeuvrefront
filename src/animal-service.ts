import axios from "axios";
import { Animal } from "./entities";


export const animalService ={
async fetchOneAnimal(id:number|string) {
    const response = await axios.get<Animal>('api/animal/'+id);
    return response.data ;
},

async fetchAllAnimal() {
    const response = await axios.get<Animal[]>('/api/animal');
    return response.data ;
},

async fetchAllAnimalByCircuit(circuitId:number|string) {
    const response = await axios.get<Animal[]>('/api/circuit/'+circuitId+'/animals');
    return response.data ;
},

async postAnimal(animal:Animal){
    const response = await axios.post<Animal>('/api/animal');
    return response.data ;
},

async updateAnimal(animal:Animal){
    const response = await axios.put<Animal>('/api/animal/'+animal.id);
    return response.data ;
},

async deleteAnimal(id:any){
    const response = await axios.delete<Animal>('/api/animal/+id');
    return response.data ;
},
}