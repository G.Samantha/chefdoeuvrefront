import axios from "axios";
import { User } from "./entities";

export async function register(user: User) {
  const response = await axios.post<User>("/api/user", user);
  return response.data;
}
