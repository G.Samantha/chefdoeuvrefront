import { Circuit } from "@/entities";
import React from "react";

interface Props {
  circuits: Circuit;
}

export default function SectionCircuit({ circuits }: Props) {
  return (
    <div className="sectionCircuit circuitSectionBackground ">
    <div>
      <h2>{circuits.name}</h2>
      <p>
      {circuits.description}
      </p>
    </div>
    <div className="imgDiv">
    <img src={circuits.img} alt="photo d'une forêt"/>
    </div>
  </div>
  );
}



