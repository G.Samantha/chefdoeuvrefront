import { Card } from "antd";
import { Reservation } from "@/entities";
import dayjs from "dayjs";

interface Props {
  reservations: Reservation;
}

export default function CardReservation({ reservations }: Props) {
  const dateString = dayjs(reservations.date).format('YYYY-MM-DD');
  
  return (
    <>
      <Card
        hoverable
        className="resaCard">
        <h5 style={{ fontSize: "x-large" }}></h5>
        <p>{reservations.user?.name}</p>
        <p>{reservations.circuit?.name}</p>
        <p>{reservations.animal?.name}</p>
        <p>{dayjs(reservations.date).format('DD-MM-YYYY')}</p>
      </Card>
    </>
  );
}
