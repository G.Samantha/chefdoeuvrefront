import React from 'react';

interface ImageProps {
  src: any;
  alt: string;
  className?: string;
}

export default function ImageComponent({ src, alt, className }:ImageProps) {
  return <img src={src} alt={alt} className={className}/>;
};

