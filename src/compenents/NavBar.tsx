import React, { useContext, useEffect, useState } from "react";
import { Layout, Menu, Button, Drawer, Row, Col } from "antd";
import {
  HomeOutlined,
  UserOutlined,
  MenuOutlined,
  EnvironmentOutlined,
  BugOutlined,
  LogoutOutlined,
  FileSearchOutlined,
  LoginOutlined,
  FormOutlined,
} from "@ant-design/icons";
import { fetchUser } from "@/auth/auth-service";
import { User } from "@/entities";
import { AuthContext } from "@/auth/auth-context";
import ImageComponent from "./ImageComponent";
import Logo from "../../public/lama.webp";

export default function NavBar() {
  const [visible, setVisible] = useState(false);
  const { token, setToken } = useContext(AuthContext);
  const [user, setUser] = useState<User>();
  const { Header } = Layout;

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [token]);

  return (
    <Layout className="layout">
      <Header className="headerNavBar">
        <Row justify="center" align="middle">
          <Col xs={10} sm={10} md={3}>
            <div className="logo">
              <ImageComponent
                src={Logo.src}
                alt="Image d'un lama avec une écharpe"
                className="imgLogo"
              />
              Alpadou
            </div>
          </Col>
          <Col xs={0} sm={0} md={15}>
            <Menu className="menuNavBar" mode="horizontal">
              <Menu.Item key="1" icon={<HomeOutlined />}>
                <a href="/">Accueil</a>
              </Menu.Item>
              <Menu.Item key="2" icon={<BugOutlined />}>
                <a href="/animal">Animaux </a>
              </Menu.Item>
              <Menu.Item key="3" icon={<EnvironmentOutlined />}>
                <a href="/circuit">Circuits</a>
              </Menu.Item>
              <Menu.Item key="4" icon={<FileSearchOutlined />}>
                <a href="/reservation">Réserver</a>
              </Menu.Item>

              <Menu.Item key="5" icon={<UserOutlined />}>
                <a href="/account">Mon compte</a>
              </Menu.Item>
              <Menu.Item
                key="6"
                style={{
                  marginRight: "10px",
                  display: user != undefined ? "none" : "flex",
                }}
                icon={<LoginOutlined />}
              >
                <a href="/login">Se connecter</a>
              </Menu.Item>

              <Menu.Item
                key="7"
                style={{ display: user != undefined ? "none" : "flex" }}
                icon={<FormOutlined />}
              >
                <a type="text" href="/register">
                  S'inscrire
                </a>
              </Menu.Item>

              <Menu.Item
                key="8"
                style={{ display: user != undefined ? "flex" : "none" }}
                icon={<LogoutOutlined />}
              >
                <a href="/" onClick={() => setToken(null)}>
                  Deconnexion
                </a>
              </Menu.Item>
            </Menu>
          </Col>

          {/* menu burger */}
          <Col xs={2} sm={2} md={0}>
            <Button type="primary" onClick={showDrawer}>
              <MenuOutlined />
            </Button>
          </Col>
        </Row>
        <Drawer
          title="Menu"
          placement="right"
          onClick={onClose}
          onClose={onClose}
          open={visible}
        >
          <Menu mode="vertical">
            <Menu.Item key="1" icon={<HomeOutlined />}>
              <a href="/">Accueil</a>
            </Menu.Item>
            <Menu.Item key="2" icon={<BugOutlined />}>
              <a href="/animal">Animaux </a>
            </Menu.Item>
            <Menu.Item key="3" icon={<EnvironmentOutlined />}>
              <a href="/circuit">Circuits</a>
            </Menu.Item>
            <Menu.Item key="4" icon={<FileSearchOutlined />}>
              <a href="/reservation">Réserver</a>
            </Menu.Item>

            <Menu.Item key="5" icon={<UserOutlined />}>
              <a href="/account/">Mon compte</a>
            </Menu.Item>
            <Menu.Item
              key="6"
              style={{
                marginRight: "10px",
                display: user != undefined ? "none" : "flex",
              }}
              icon={<LoginOutlined />}
            >
              <a href="/login">Se connecter</a>
            </Menu.Item>

            <Menu.Item
              key="7"
              style={{ display: user != undefined ? "none" : "flex" }}
              icon={<FormOutlined />}
            >
              <a type="text" href="/register">
                S'inscrire
              </a>
            </Menu.Item>
            <Menu.Item
              key="8"
              style={{ display: user != undefined ? "flex" : "none" }}
              icon={<LogoutOutlined />}
            >
              <a href="/" onClick={() => setToken(null)}>
                Deconnection
              </a>
            </Menu.Item>
          </Menu>
        </Drawer>
      </Header>
    </Layout>
  );
}
