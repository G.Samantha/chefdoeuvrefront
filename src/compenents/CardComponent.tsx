import ImageComponent from "./ImageComponent";


interface CardProps {
  name: string | undefined;
  src: any;
  description: string | undefined;
}

export default function CardComponent({src, name, description}: CardProps) {
  return (
    <>
      <div className="card">
        <ImageComponent src={src} alt="" className="card-img" />
        <div className="card-body">
          <div>
            <h5 className="card-title">{name}</h5>
          </div>
          <div className="card-info">
            <p >{description}</p>
          </div>
        </div>
      </div>
    </>
  );
}