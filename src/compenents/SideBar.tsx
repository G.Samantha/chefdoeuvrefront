import { AuthContext } from "@/auth/auth-context";
import { Button, Drawer, Layout, Menu } from "antd";
import { useRouter } from "next/router";
import { useContext} from "react";
import {
  SettingOutlined,
  BookOutlined,
    LogoutOutlined,
  } from "@ant-design/icons";


export default function Sidebar() {
  const { token, setToken } = useContext(AuthContext);
  const router = useRouter();

  
  return (
    <Layout className="layout sideBar" >
    <Menu mode="vertical" className="sidebarMenu">
      <Menu.Item key="1" icon={<BookOutlined/>} >
        <a href="/account/historique">Historique</a>
      </Menu.Item>
      <Menu.Item key="2" icon={<SettingOutlined />}>
        <a href="/account/info">Réglage</a>
      </Menu.Item>
       <Menu.Item key="3" icon={<LogoutOutlined/>}>
        <a href="/" onClick={() => setToken(null)}>Deconnection</a>
      </Menu.Item> 
    </Menu>
    </Layout>
  );
}



