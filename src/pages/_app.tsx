import "@/styles/globals.css";
import axios from "axios";
import type { AppProps } from "next/app";
import "../auth/axios-config";
import NavBar from "@/compenents/NavBar";
import { AuthContextProvider } from "@/auth/auth-context";
import Footer from "@/compenents/Footer";
import React from "react";
import LoadingPage from "@/compenents/LoadingPage";

axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AuthContextProvider>
      <NavBar/>
      <Component {...pageProps} />
      <Footer/>
    </AuthContextProvider>
  );
}
