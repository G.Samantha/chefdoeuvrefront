import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { User } from "@/entities";
import { Button, Card, Form, Input } from "antd";
import { useRouter } from "next/router";
import { useContext, useState } from "react";
import { red } from "@ant-design/colors";
import Link from "next/link";

export default function loginPage() {
  const router = useRouter();
  const { setToken } = useContext(AuthContext);
  const [error, setError] = useState("");

  async function handleSubmit(values: User) {
    setError("");
    try {
      setToken(await login(values.email, values.password));
      router.push("/account");
    } catch (error: any) {
      if (error.response.status == 401) {
        router.push("/login");
      } else if (error.response.status == 404) {
        router.push("/404");
      }
    }
  }

  return (
    <>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: " center",
        }}
      >
        <div>
          <h1 style={{ textAlign: "center", margin: "3%" }}>
            Veuillez vous connectez à votre compte
          </h1>
        </div>

        <Card style={{ width: "auto", margin: "20px", textAlign: "center", backgroundColor: "#80808012"}}>
          <h2 style={{ paddingBottom: "2%" }}>Login</h2>
          {error && <p style={{ color: red.primary }}>{error}</p>}
          <Form onFinish={handleSubmit}>
            <Form.Item
              label="Email"
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input type="email" />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item>
              <Button className="buttonYellow" htmlType="submit">
                Se connecter
              </Button>
            </Form.Item>
            <Link href={"/register"}>S'inscire</Link>
          </Form>
        </Card>
      </div>
    </>
  );
}
