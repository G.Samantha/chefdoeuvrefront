import React from "react";
import { Button } from "antd";
import imgAccueil from "../../public/accueilChien.webp"
import ImageComponent from "@/compenents/ImageComponent";

export default function index() {
  return (
    <div>
      <div className="indexSection  bgIndex">
        <div className="divTitle">
          <h1 className="title">L'aventure Alpadou vous attends</h1>
        </div>
      </div>

      <div className="sectionAccueil firstSectionBackground ">
        <div>
          <h2>Nos animaux</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <div className="accueilDivBouton">
            <Button
              href={"/animal/"}
              className="buttonYellow"
            >
              Voir plus
            </Button>
          </div>
        </div>
        <div className="imgDiv">
        <ImageComponent src={imgAccueil.src} alt="image" className="imgAccueilSection"/>
        </div>
      </div>

      <div className="sectionAccueil firstSectionBackground ">
        <div>
          <h2>Les circtuits chez Alpadou</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <div className="accueilDivBouton">
            <Button
              href={"/circuit/"}
              className="buttonYellow"
            >
              Voir plus
            </Button>
          </div>
        </div>
        <div className="imgDiv">
        <ImageComponent src={imgAccueil.src} alt="image" className="imgAccueilSection"/>
        </div>
      </div>

      <div className="banner">
        <div className="banner-text">
          <h2>Réservation</h2>
          <p>
            Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <div className="buttonDivBanner">
            <Button className="buttonBanner">
              <a href={"/reservation/"}>
                Voir plus
              </a>
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
}
