import { User } from "@/entities";
import React, { useEffect, useState } from "react";
import { Card } from "antd";
import { useRouter } from "next/router";
import { fetchAdmin} from "@/auth/auth-service";


export default function AdminPage() {
  const [admin, setAdmin] = useState<User>();
  const router = useRouter();

  useEffect(() => {
    fetchAdmin()
      .then((data) => {
        setAdmin(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
        if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);
  
  return (
    <>
      <div style={accountStyle}>
        <div style={accountBodyStyle}>
          <Card>
            <h2 style={TitleCardAccountIndexStyle}>Mon compte</h2>
            <div style={{ width: 18 + "rem" }}>
              <div>
                <p>Nom: {admin?.name}</p>
                <p> Prénom:{admin?.lastname}</p>
                <p>E-mail: {admin?.email} </p>
                <p>Téléphone : {admin?.phone}</p>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </>
  );
}

const accountStyle: React.CSSProperties = {
  display: "flex",
};

const accountBodyStyle: React.CSSProperties = {
  display: "flex",
  flexDirection: "column",
  margin: "1% auto 0% 5%",
};

const TitleCardAccountIndexStyle: React.CSSProperties = {
  textAlign: "center",
};


