import { Reservation, User } from "@/entities";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { fetchUser } from "@/auth/auth-service";
import CardReservation from "@/compenents/CardReservation";
import { ReservationService } from "@/reservation-service";
import SideBar from "@/compenents/SideBar";

// interface Props {
//   reservations: Reservation[];
// }

export default function Historique() {
  const [user, setUser] = useState<User>();
  const [reservations, setReservations] = useState<Reservation[]>();
  const router = useRouter();
  // const { token, setToken } = useContext(AuthContext);

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        } else if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  useEffect(() => {
    if (user && "id" in user && typeof user.id === "number") {
      ReservationService.fetchReservationsByUser(user.id)
        .then((data) => {
          setReservations(() => data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  }, [user]);

  return (
    <>
      <div className="historiqueBody">
        <SideBar />
        <div className="containerHistory">
          <h1>Historique de vos commandes</h1>
          <div className="historiqueContainerCard">
            <div className="historiqueCard">
              {reservations &&
                reservations.map((item) => (
                  <CardReservation key={item.id} reservations={item} />
                ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
