import Sidebar from "@/compenents/SideBar";
import { Button, Card } from "antd";
import { User } from "@/entities";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { fetchUser, updateUser } from "@/auth/auth-service";
import FormEditUser from "@/compenents/FormEditUser";

export default function Info() {
  const [user, setUser] = useState<User>();
  const [showEdit, setShowEdit] = useState(false);
  const router = useRouter();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
        else if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  function toggle() {
    setShowEdit(!showEdit);
  }

  async function update(user: User) {
    const updated = await updateUser(user);
    setUser(updated);
  }

  return (
    <>
      <div className="accountContainerInfo">
        <Sidebar />
        <div className="accountInfoBody">
          <Card hoverable bodyStyle={{padding: "0"}} className="InfoCard">
            <h1 style={{ fontSize: "x-large" }}></h1>
            <p>Nom: {user?.name}</p>
            <p>Prénom: {user?.lastname}</p>
            <p>E-mail: {user?.email}</p>
            <p>Téléphone: {user?.phone}</p>
            <Button onClick={toggle} className="buttonEditStyle">
              Modifier
            </Button>
          </Card>
          <div>
            {showEdit && <FormEditUser edited={user} onSubmit={update} />}
          </div>
        </div>
      </div>
    </>
  );
}
