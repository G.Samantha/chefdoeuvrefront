import { User } from "@/entities";
import React, { useEffect, useState } from "react";
import SideBar from "@/compenents/SideBar";
import { Card } from "antd";
import { useRouter } from "next/router";
import { fetchUser } from "@/auth/auth-service";


export default function UserPage({}) {
  const [user, setUser] = useState<User>();
  const router = useRouter();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
        else if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);
  
  return (
    <div>
      <div className="accountContainer">
        <SideBar/>
        <div className="accountBody" >
          <Card className="cardBodyAccount">
            <h1 className="accountTitle">Mon compte</h1>
            <div>
              <div>
                <p>Nom: {user?.name}</p>
                <p> Prénom:{user?.lastname}</p>
                <p>E-mail: {user?.email} </p>
                <p>Téléphone : {user?.phone}</p>
              </div>
            </div>
          </Card>
        </div>
      </div>
    </div>
  );
}



