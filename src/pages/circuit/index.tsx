import { circuitService } from "@/circuit-service";
import SectionCircuit from "@/compenents/SectionCircuit";
import { Circuit } from "@/entities";
import { GetServerSideProps } from "next";
import React from "react";

interface Props {
  circuits: Circuit[];
}

export default function CircuitPage({ circuits }: Props) {
  return (
    <>
    <div className="divTextCircuitPage">
        <h1>Les voyages d'Alpadou</h1>
        <p className="paraCircuitPage">
          Voici nos circuits que nous vous proposons. Tous sont adpatés aux adultes comme aux petits.
        </p>
      </div>
      <div className="divContainerCircuit">
        {circuits.map((item) => (
          <SectionCircuit key={item.id} circuits={item} />
        ))}
      </div>
      
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      circuits: await circuitService.fetchAllCircuit(),
    },
  };
};
