import { GetServerSideProps } from "next";
import React from "react";
import { Animal } from "@/entities";
import { animalService } from "@/animal-service";
import CardComponent from "@/compenents/CardComponent";

interface Props {
  animals: Animal[];
}

export default function AnimalPage({ animals }: Props) {
  return (
    <>
      <div className="divTextAnimalPage">
        <h1>Les compagnons d'Alpadou</h1>
        <p className="paraAnimalPage">
          Voici une liste de nos animaux qui pourrons vous accompagner durant
          vos aventures dans notre ferme. Veuillez passez la souris ou cliquer
          sur les cards !
        </p>
      </div>
      <div
        className="divContainerCardAnimal"
      >
        {animals.map((item) => (
          <CardComponent
            key={item.id}
            name={item.name}
            src={item.img}
            description={item.description}
          />
        ))}
      </div>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      animals: await animalService.fetchAllAnimal(),
    },
  };
};
