import { AuthContext } from "@/auth/auth-context";
import { fetchUser, login } from "@/auth/auth-service";
import { register } from "@/register-service";
import { User } from "@/entities";
import { Button, Card, Form, Input, Modal } from "antd";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

export default function Register() {
  const router = useRouter();
  const { setToken } = useContext(AuthContext);
  const [user, setUser] = useState<User>();

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/register");
        } else if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  async function handleSubmit(values: User) {
    try {
      await register(values);
      showConfirm();
      setToken(await login(values.email, values.password));
      router.push("/account/");
    } catch (returnedError: any) {
      showError(returnedError.response?.data);
    }
  }

  function showConfirm() {
    Modal.success({
      title: "Compte créé",
      content: `Votre compte a bien été créé`,
      onOk() {},
    });
  }

  function showError(error: any) {
    Modal.success({
      title: error.title,
      content: error.detail,
      onOk() {
      },
    });
  }

  return (
    <>
      <div className="divContainerRegister">
        <h1>
          Formulaire de création de compte
        </h1>

        <Card
          className="CardBodyRegister"
        >
          <h2 className="h2Register">
            Créer un compte
          </h2>
          <Form onFinish={handleSubmit}>
            <Form.Item
              label="Nom"
              name="name"
              rules={[{ required: true, message: "Please input your Name!" }]}
            >
              <Input type="name" />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input type="email" />
            </Form.Item>

            <Form.Item
              label="Mot de passe"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item>
              <Button className="buttonYellow" htmlType="submit"
              >
                Créer
              </Button>
            </Form.Item>
          </Form>
        </Card>
        <div className="rgpdLink">
          <p>Alpadou s'engage à ne recueillir que les données nécessaires et d'être conforme aux bonnes pratiques de la CNIL. 
          <br/><a href="https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles"> En savoir plus sur la gestion de vos données et vos droits</a>
          </p>
        </div>
      </div>
    </>
  );
}
