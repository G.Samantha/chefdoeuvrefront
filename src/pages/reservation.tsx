import React, { useContext, useEffect, useState } from "react";
import type { DatePickerProps } from "antd";
import { Button, DatePicker, Form, Modal, Select, Space } from "antd";
import { Animal, Circuit, User } from "@/entities";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { fetchUser } from "@/auth/auth-service";
import { RangePickerProps } from "antd/es/date-picker";
import dayjs from "dayjs";
import { ReservationService } from "@/reservation-service";
import { animalService } from "@/animal-service";
import { circuitService } from "@/circuit-service";

interface Props {
  circuits: Circuit[];
}

export default function Reservation({ circuits }: Props) {
  const { Option } = Select;
  const [form] = Form.useForm();
  const [circuitSelected, setCircuitSelected] = useState<number | undefined>();
  const [animalSelected, setAnimaltSelected] = useState<number | undefined>();
  const [animalList, setAnimalList] = useState<Array<Animal> | undefined>();
  const [user, setUser] = useState<User>();
  const router = useRouter();

  function showConfirm() {
    let secondsToGo = 5;
    const modal = Modal.success({
      title: "Réservation réussite",
      content: `Réservation réussite cliquer pour accéder à votre page commande ${secondsToGo}`,
      onOk() {
        router.push("/account/historique");
      },
    });
    const timer = setInterval(() => {
      secondsToGo -= 1;
      modal.update({
        content: `Réservation réussite cliquer pour accéder à votre page commande ${secondsToGo}`,
      });
    }, 1000);
    setTimeout(() => {
      clearInterval(timer);
      modal.destroy();
    }, secondsToGo * 1000);
  }

  useEffect(() => {
    fetchUser()
      .then((data) => {
        setUser(data);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          router.push("/login");
        }
        else if (error.response.status == 404) {
          router.push("/404");
        }
      });
  }, []);

  const onReservationComplete = async (values: any) => {
    const reservation = {
      date: values.date,
      animal: values.animal,
      circuit: values.circuit,
      user: user?.id,
    };
    console.log(values);

    const response = await ReservationService.postReservation(reservation);
  };

  const onChangeDate: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };

  const onChangeCircuit = async (circuitId: number) => {
    setCircuitSelected(circuitId);
    const response = await animalService.fetchAllAnimalByCircuit(circuitId);
    setAnimalList(response);
  };

  const disabledDate: RangePickerProps["disabledDate"] = (current) => {
    return current && current < dayjs().endOf("day");
  };

  return (
    <div className="resaFormStyle">
      <div className="sectiontextResa">
        <h1>Réservation</h1>
        <p>
          Veuillez séléctionner votre circuit, votre futur compagnon de route et
          la date souhaitez. Pour toute question n'hésité pas à nous contacter
        </p>
      </div>
      <Form
        form={form}
        name="control-hooks"
        onFinish={onReservationComplete}
        className="formChoice"
      >
        <Form.Item name="circuit" label="Circuit" rules={[{ required: true }]}>
          <Select
            placeholder="Select a option and change input text above"
            allowClear
            onChange={onChangeCircuit}
          >
            {circuits.map((circuit) => (
              <Option key={'circuit'+circuit.id} value={circuit.id}>{circuit.name}</Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          name="animal"
          label="Animal"
          rules={[{ required: true }]}
          style={{ display: animalList != undefined ? "flex" : "none" }}
        >
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            {animalList &&
              animalList.map((animal) => (
                <Option key={'animal'+animal.id} value={animal.id}>{animal.name}</Option>
              ))}
          </Select>
        </Form.Item>

        <Form.Item name="date" label="Date" rules={[{ required: true }]}>
          <DatePicker
            format="MM/D/YYYY"
            onChange={onChangeDate}
            disabledDate={disabledDate}
          />
        </Form.Item>

        <Form.Item className="buttonContainer">
          <Button
            className="buttonYellow"
            type="primary"
            htmlType="submit"
            onClick={showConfirm}
          >
            Valider
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
  return {
    props: {
      circuits: await circuitService.fetchAllCircuit(),
    },
  };
};
