import axios from "axios";
import {Circuit} from "./entities";

export const circuitService = {

    async fetchOneCircuit(id:number|string) {
        const response = await axios.get<Circuit>('api/circuit/'+id);
        return response.data ;
    },
    
    async fetchAllCircuit() {
        const response = await axios.get<Circuit[]>('/api/circuit');
        return response.data ;
    },
    
    async postCircuit(Circuit:Circuit){
        const response = await axios.post<Circuit>('/api/circuit', Circuit);
        return response.data ;
    },
    
    async updateCircuit(Circuit:Circuit){
        const response = await axios.put<Circuit>('/api/circuit'+Circuit.id, + Circuit,);
        return response.data ;
    },
    
    async deleteCircuit(id:any){
        const response = await axios.delete<Circuit>('/api/circuit/+id');
        return response.data ;
    },



}